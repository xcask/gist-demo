package pt.jcalado.gists.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import pt.jcalado.gists.data.Gist
import pt.jcalado.gists.repository.GistRepository

class GitDetailViewModel(val repository: GistRepository) : ViewModel() {
    fun getGistDetail(id: String): LiveData<Gist> = repository.get(id)
}
