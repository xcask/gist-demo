package pt.jcalado.gists.ui.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import kotlinx.android.synthetic.main.gist_detail_layout.*
import kotlinx.android.synthetic.main.gist_list_item.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import pt.jcalado.gists.R
import pt.jcalado.gists.data.Gist
import pt.jcalado.gists.utils.DateUtils


class GistDetailActivity : AppCompatActivity() {
    private var shouldFinishAnimated: Boolean = false
    private var gistId: String? = null
    private val viewModel: GitDetailViewModel by viewModel()

    companion object {
        const val EXTRA_GIST = "pt.gists.detail.extra_gist"
        const val EXTRA_GIST_ID = "pt.gists.detail.extra_gist_id"
        const val EXTRA_SHOULD_FINISH_WITH_TRANSITION =
            "pt.gists.detail.extra_gist_finish_transition"


        fun start(activity: Activity, activityOptionsCompat: ActivityOptionsCompat?, gist: Gist) {
            val i = Intent(activity, GistDetailActivity::class.java)
            i.putExtra(EXTRA_GIST, gist)
            i.putExtra(EXTRA_SHOULD_FINISH_WITH_TRANSITION, activityOptionsCompat != null)
            activity.startActivity(i, activityOptionsCompat?.toBundle())
        }

        fun start(activity: Activity, gistId: Int) {
            val i = Intent(activity, GistDetailActivity::class.java)
            i.putExtra(EXTRA_GIST_ID, gistId)
            activity.startActivity(i)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupUI()
        setupData()
        setupObservation()
    }

    override fun onStop() {
        super.onStop()

        if (shouldFinishAnimated)
            ActivityCompat.finishAfterTransition(this)
    }

    private fun setupData() {
        when {
            intent.extras?.containsKey(EXTRA_GIST) == true -> {
                val gist = intent.extras?.get(EXTRA_GIST) as Gist?
                gist?.let {
                    setupGist(it)
                    gistId = it.id
                }

                shouldFinishAnimated = intent?.getBooleanExtra(
                    EXTRA_SHOULD_FINISH_WITH_TRANSITION,
                    false
                ) ?: false
            }
            intent.extras?.containsKey(EXTRA_GIST_ID) == true -> {
                gistId = intent.extras?.getString(EXTRA_GIST_ID)
            }
            else -> //an error screen could be shown
                throw IllegalArgumentException("Missing necessary extras to display and obtain data.")
        }
    }

    private fun setupGist(gist: Gist) {
        supportActionBar?.title = gist.id?.substring(0, 10)
        gist_name.text = gist.id?.substring(0, 10)
        gist_date.text = DateUtils.getSimpleFormat(gist.createdAt)
        gist_user_name.text = gist.owner?.userName
        gist_description.text = gist.description

        Glide.with(this)
            .load(gist.imageUrl)
            .into(gist_image)

        Glide.with(this)
            .load(gist.owner?.avatarUrl)
            .transform(CircleCrop())
            .placeholder(R.drawable.ic_person_black_24dp)
            .error(R.drawable.ic_person_black_24dp)
            .into(gist_user_image)
    }

    private fun setupObservation() {
        gistId?.let { id ->
            viewModel.getGistDetail(id).observe(this, Observer { gist ->
                setupGist(gist)
            })
        }
    }

    private fun setupUI() {
        setContentView(R.layout.gist_detail_layout)

        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home && shouldFinishAnimated) {
            ActivityCompat.finishAfterTransition(this)
            true
        } else
            super.onOptionsItemSelected(item)
    }
}
