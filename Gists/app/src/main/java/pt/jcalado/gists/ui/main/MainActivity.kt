package pt.jcalado.gists.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.main_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import pt.jcalado.gists.R
import pt.jcalado.gists.ui.detail.GistDetailActivity
import pt.jcalado.gists.ui.main.adapters.GistsAdapter
import androidx.core.app.ActivityOptionsCompat
import android.view.View
import pt.jcalado.gists.data.Gist


class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModel()
    private lateinit var gistsAdapter: GistsAdapter

    companion object {
        fun start(activity: Activity) {
            val i = Intent(activity, MainActivity::class.java)
            activity.startActivity(i)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupUI()
        setupObservation()
    }

    private fun setupObservation() {
        viewModel.gists.observe(this, Observer {
            gistsAdapter.setGists(it)

        })
    }

    private fun setupUI() {
        setContentView(R.layout.main_layout)
        gistsAdapter = GistsAdapter(mutableListOf()) { gist: Gist, view: View ->
            val activityOptionsCompat =
                ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, "gistContainer")
            GistDetailActivity.start(this, activityOptionsCompat, gist)
        }
        main_gist_list.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
                    ContextCompat.getDrawable(context, R.drawable.space_divider)?.let {
                        setDrawable(it)
                    }
                }
            )
            adapter = gistsAdapter
        }
    }
}
