package pt.jcalado.gists.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import pt.jcalado.gists.R
import pt.jcalado.gists.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prepareUI()
    }

    private fun prepareUI() {
        setContentView(R.layout.splash_layout)
        scheduleNextScreen()
    }

    private fun scheduleNextScreen() {
        val exitDelay = 2000L
        Handler().postDelayed(
            {
                routeToNextScreen()
                finish()
            }
            , exitDelay
        )
    }

    private fun routeToNextScreen() {
        MainActivity.start(this)
    }
}