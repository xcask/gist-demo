package pt.jcalado.gists.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object DateUtils {
    private const val SIMPLE_DATE_HOUR_PATTERN = "dd LLLL yyyy HH:mm"
    private const val SERVER_DATE_PATTERN = "uuuu-MM-dd'T'HH:mm:ss'Z'"


    fun getSimpleFormat(date: LocalDateTime)
            : String = date.format(DateTimeFormatter.ofPattern(SIMPLE_DATE_HOUR_PATTERN))

    fun toServerFormat(date: LocalDateTime)
            : String = date.format(DateTimeFormatter.ofPattern(SERVER_DATE_PATTERN))

    fun toLocalDateTime(date: String?)
            : LocalDateTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(SERVER_DATE_PATTERN))
}
