package pt.jcalado.gists.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pt.jcalado.gists.data.Gist

@Database(entities = [Gist::class], version = 1)
@TypeConverters(Converters::class)
abstract class GistDatabase : RoomDatabase() {
    abstract fun gistDAO(): GistDAO
}