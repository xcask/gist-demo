package pt.jcalado.gists.api

import okhttp3.OkHttpClient
import pt.jcalado.gists.BuildConfig
import pt.jcalado.gists.api.services.GistService
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class GistServiceImpl {
    companion object {
        private const val BASE_URL = BuildConfig.API_BASE_URL

        //TODO DI all of these
        fun getGistService(
            moshiConverterFactory: MoshiConverterFactory, client: OkHttpClient
        ): GistService {
            val retrofit =
                Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(moshiConverterFactory)
                    .client(client)
                    .build()
            return retrofit.create(GistService::class.java)
        }
    }
}