package pt.jcalado.gists.utils

object ImageUtils {
    fun getImage(imageId: Int): String = "https://picsum.photos/id/$imageId/1080/720"

}
