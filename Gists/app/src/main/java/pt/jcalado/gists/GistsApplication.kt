package pt.jcalado.gists

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import pt.jcalado.gists.di.appModule

class GistsApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        setupKoin()
    }

    /**
     * SETUP DEPENDENCIES INJECTION
     */
    private fun setupKoin() {
        startKoin{
            androidLogger()
            androidContext(this@GistsApplication)
            modules(appModule)
        }
    }
}