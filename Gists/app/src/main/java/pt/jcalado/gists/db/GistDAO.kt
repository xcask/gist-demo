package pt.jcalado.gists.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import pt.jcalado.gists.data.Gist

@Dao
interface GistDAO {
    @Insert(onConflict = IGNORE)
    fun save(gist: Gist)

    @Insert(onConflict = IGNORE)
    fun saveAll(gist: List<Gist>)

    @Query("SELECT * from gist WHERE id = :gistId")
    fun load(gistId: String) : LiveData<Gist>

    @Query("SELECT * from gist")
    fun loadAll(): LiveData<List<Gist>>
}