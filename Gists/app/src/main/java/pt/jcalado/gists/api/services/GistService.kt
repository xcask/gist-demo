package pt.jcalado.gists.api.services

import pt.jcalado.gists.data.Gist
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GistService {
    @GET("/gists/public")
    fun getAll(): Call<List<Gist>>

    @GET("/gists/{gist_id}")
    fun get(@Path("gist_id") gistId: String): Call<Gist>
}