package pt.jcalado.gists.repository

import androidx.lifecycle.LiveData
import pt.jcalado.gists.api.services.GistService
import pt.jcalado.gists.data.Gist
import pt.jcalado.gists.db.GistDAO
import pt.jcalado.gists.utils.ImageUtils
import java.util.concurrent.Executor

class GistRepository(
    private val webService: GistService,
    private val gistDAO: GistDAO,
    private val executor: Executor
) {

    fun getAll(): LiveData<List<Gist>> {
        executor.execute {
            val savedData = gistDAO.loadAll()

            if (savedData.value == null) {
                // Refreshes the data.
                val response = webService.getAll().execute()

                response.body()?.forEach {
                    it.imageUrl = ImageUtils.getImage(Gist.getImageIndex())
                }

                // Check for errors here.
                //TODO

                gistDAO.saveAll(response.body()!!)
            }
        }

        return gistDAO.loadAll()
    }

    fun get(id: String): LiveData<Gist> {
        executor.execute {
            val savedData = gistDAO.load(id)

            if (savedData.value == null) {
                // Refreshes the data.
                val response = webService.get(id).execute()

                //troublesome code is generating a different image for this gist
                response.body()?.let {
                    it.imageUrl = ImageUtils.getImage(Gist.getImageIndex())
                }

                // Check for errors here.
                //TODO

                gistDAO.save(response.body()!!)
            }
        }

        return gistDAO.load(id)
    }

}