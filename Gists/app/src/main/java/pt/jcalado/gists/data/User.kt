package pt.jcalado.gists.data

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

data class User(
    @field:Json(name = "userId") val userId: Int,
    @field:Json(name = "login") val userName: String?,
    @field:Json(name = "avatar_url") val avatarUrl: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel) {
        writeInt(userId)
        writeString(userName)
        writeString(avatarUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}