package pt.jcalado.gists.data

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import pt.jcalado.gists.utils.DateUtils
import java.time.LocalDateTime

@Entity
data class Gist(
    @PrimaryKey @NonNull @field:Json(name = "id") val id: String,
    @field:Json(name = "created_at") val createdAt: LocalDateTime,
    @field:Json(name = "description") val description: String?,
    @Embedded @field:Json(name = "owner") val owner: User?,
    var imageUrl: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        DateUtils.toLocalDateTime(parcel.readString()),
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel) {
        writeString(id)
        writeString(DateUtils.toServerFormat(createdAt))
        writeString(description)
        writeParcelable(owner, flags)
        writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Gist> {
        var imageIndexCounter = 1

        override fun createFromParcel(parcel: Parcel): Gist {
            return Gist(parcel)
        }

        override fun newArray(size: Int): Array<Gist?> {
            return arrayOfNulls(size)
        }

        //TODO custom getter?
        fun getImageIndex(): Int {
            imageIndexCounter = imageIndexCounter.inc()
            return imageIndexCounter
        }
    }
}