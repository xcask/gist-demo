package pt.jcalado.gists.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import pt.jcalado.gists.data.Gist
import pt.jcalado.gists.repository.GistRepository

class MainViewModel(val repository: GistRepository) : ViewModel() {
    val gists : LiveData<List<Gist>> = repository.getAll()
}
