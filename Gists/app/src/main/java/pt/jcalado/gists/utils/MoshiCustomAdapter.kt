package pt.jcalado.gists.utils

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.LocalDateTime

class MoshiLocalDateAdapter {
    @ToJson
    fun toJson(date: LocalDateTime): String = DateUtils.toServerFormat(date)

    @FromJson
    fun fromJson(date: String): LocalDateTime = DateUtils.toLocalDateTime(date)
}