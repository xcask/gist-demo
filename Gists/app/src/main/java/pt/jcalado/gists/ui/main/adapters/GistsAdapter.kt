package pt.jcalado.gists.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import pt.jcalado.gists.R
import pt.jcalado.gists.data.Gist
import pt.jcalado.gists.utils.DateUtils


class GistsAdapter(private val gists: MutableList<Gist>, private val listener: (Gist, View) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : RecyclerView.ViewHolder = GistsViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.gist_list_item, parent, false)
    )


    override fun getItemCount(): Int = gists.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is GistsViewHolder)
            holder.bind(gists[position], position, listener)
    }

    fun setGists(it: List<Gist>?) {
        //TODO should be adding newer items that come from pagination
        gists.clear()
        it?.let {
            gists.addAll(it)
            notifyDataSetChanged()
        }
    }

    class GistsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = itemView.findViewById(R.id.gist_name)
        private val image: ImageView = itemView.findViewById(R.id.gist_image)
        private val userImage: ImageView = itemView.findViewById(R.id.gist_user_image)
        private val userName: TextView = itemView.findViewById(R.id.gist_user_name)
        private val addedDate: TextView = itemView.findViewById(R.id.gist_date)

        fun bind(item: Gist, position: Int, listener: (Gist, View) -> Unit) = with(itemView) {
            setOnClickListener { listener(item, itemView) }

            name.text = item.id?.substring(0, 10)
            addedDate.text = DateUtils.getSimpleFormat(item.createdAt)
            userName.text = item.owner?.userName

            Glide.with(context)
                .load(item.imageUrl)
                .into(image)

            Glide.with(context)
                .load(item.owner?.avatarUrl)
                .transform(CircleCrop())
                .placeholder(R.drawable.ic_person_black_24dp)
                .error(R.drawable.ic_person_black_24dp)
                .into(userImage)
        }
    }
}
