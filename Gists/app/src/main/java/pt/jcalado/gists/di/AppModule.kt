package pt.jcalado.gists.di

import androidx.room.Room
import com.squareup.moshi.Moshi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pt.jcalado.gists.api.GistServiceImpl
import pt.jcalado.gists.db.GistDatabase
import pt.jcalado.gists.repository.GistRepository
import pt.jcalado.gists.ui.detail.GitDetailViewModel
import pt.jcalado.gists.ui.main.MainViewModel
import pt.jcalado.gists.utils.MoshiLocalDateAdapter
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

//TODO create separate modules
val appModule = module {

    //NETWORK
    single<Interceptor> {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }
    single { OkHttpClient.Builder().addInterceptor(get()).build() }
    single { Moshi.Builder().add(MoshiLocalDateAdapter()).build() }
    single<MoshiConverterFactory> { MoshiConverterFactory.create(get()) }

    single { GistServiceImpl.getGistService(get(), get()) }

    single { Room.databaseBuilder(get(), GistDatabase::class.java, "gist-db").build() }

    single { get<GistDatabase>().gistDAO() }

    single<Executor> { Executors.newSingleThreadExecutor() }

    single {
        GistRepository(get(), get(), get())
    }

    // Viewmodels
    viewModel { MainViewModel(get()) }
    viewModel { GitDetailViewModel(get()) }
}